# The steps below work for the "full" version
cd releases
tar xvfz ../tarballs/chapel-1.10.0.tar.gz
cd chapel-1.10.0
# Patch to build with mpicc/cxx wrappers always
cp ../../etc/build/Makefile.gnu-1.10 make/compiler/Makefile.gnu
# Clean up old detritus
env | grep CHPL | sed 's/=.*//' | while read i; do unset $i; done
source util/setchplenv.bash
export CHPL_HWLOC=none
gmake
export CHPL_COMM=gasnet
export CHPL_COMM_SUBSTRATE=mpi
gmake


