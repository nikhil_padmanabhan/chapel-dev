# The steps below work for the "full" version
mkdir -p releases
cd releases
tar xvfz ../tarballs/chapel-1.10.0.tar.gz
cd chapel-1.10.0
# Clean up old detritus
env | grep CHPL | sed 's/=.*//' | while read i; do unset $i; done
source util/setchplenv.bash
export CHPL_HOST_PLATFORM=cray-xc
export CHPL_LAUNCHER=none
export CHPL_COMM=none
gmake

