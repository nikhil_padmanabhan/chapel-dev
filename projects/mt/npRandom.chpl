const UPPER_MASK : uint(32) = 0x80000000;
const LOWER_MASK : uint(32) = 0x7fffffff;
const N = 624;
const M = 397;

// NOTES :
//   1. Do not seed the random number generator with zero
//   2. This is a 32-bit algorithm
//   3. The current version cannot be called in parallel. 
record MT19937 {

	// State of the generator
	var mti : int;
	var mt : [0.. #N] uint(32);

	// Constructor
	proc MT19937(s : uint(32) = 4357) {
		seed(s);
	}

	// Set seed
	proc seed(s : uint(32) = 4357) { // 4357 is the default seed
		mt[0] = s & 0xffffffff : uint(32);
		for i in 1..(N-1) {
			mt[i] = (1812433253 : uint(32) * (mt[i-1] ^ (mt[i-1] >> 30)) + i : uint(32));
			mt[i] &= 0xffffffff : uint(32);
		}
		mti = N;
	}

	// Generate the next N randoms
	proc generate() {
		var y : uint(32);
		const MAGIC : uint(32) = 0x9908b0df;
		// The basic case
		for kk in 0.. #(N-M) {
			y = (mt[kk] & UPPER_MASK) | (mt[kk+1] & LOWER_MASK);
			mt[kk] = mt[kk+M] ^ (y >> 1);
			if ((y % 2) != 0) then mt[kk] ^= MAGIC;
		}
		// Handle the mod M indexing
		for kk in (N-M)..(N-2) {
			y = (mt[kk] & UPPER_MASK) | (mt[kk+1] & LOWER_MASK);
			mt[kk] = mt[kk+(M-N)] ^ (y >> 1);
			if ((y % 2) != 0) then mt[kk] ^= MAGIC;
		}
		// Handle the last elt
		y = (mt[N-1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
		mt[N-1] = mt[M-1] ^ (y >> 1);
		if ((y % 2) != 0) then mt[N-1] ^= MAGIC;

		mti = 0;
	}


	// Get randoms (note that this generates N words at a time).
	//
	//  get(real) : returns a real
	//  get() : returns a uint (or if type in is not real)
	proc get(type t = uint)  {
		if (mti >= N) then generate(); 
	
		// Temper
		var k = mt[mti];
		k ^= (k >> 11);
		k ^= (k << 7) & 0x9d2c5680;
		k ^= (k << 15) & 0xefc60000;
		k ^= (k >> 18);

		mti += 1;
		if (t==real) then return k*2.3283064365386962891e-10; else return k;
	}
}

// The xorshift* random number generator discussed in 
// arXiv:1402.6246
// This is a 64-bit generator
// You should seed with a number not equal to zero.
record Xorshift64star {
	var x : uint(64);

	proc Xorshift64star(seed : uint(64) = 4357) {
		x = seed;
	}

	proc get(type t = uint) {
		var ret : uint(64);
		x ^= x >> 12;
		x ^= x << 25;
		x ^= x >> 27;
		ret = x*2685821657736338717;
		if (t==real) then return ret*5.4210108624275221700e-20; else return ret;
	}
}


record Xorshift1024star {
	var p : int;
	var s : [0.. #16] uint(64);

	proc Xorshift1024star(seed : uint(64) = 4357) {
		p = 0;
		// See with the Xorshift64star generator
		var rng = new Xorshift64star(seed);
		for s1 in s do s1 = rng.get(); 
	}

	proc get(type t = uint) {
		var s0 : uint(64) = s[p];
		p = (p+1)&15;
		var s1 : uint(64) = s[p];
		s1 ^= s1 << 31;
		s1 ^= s1 >> 11;
		s0 ^= s0 >> 30;
		s[p] = s0^s1;
		s0= s[p]*1181783497276652981;
		if (t==real) then return s0*5.4210108624275221700e-20; else return s0;
	}
}
