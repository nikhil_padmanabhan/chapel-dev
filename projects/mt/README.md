This contains my implementation of the Mersenne Twister algorithm
in Chapel. It is a literal translation of the GSL implementation, 
which is also included here for reference. 

The license in the GSL version applies.
