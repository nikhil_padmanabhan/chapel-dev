use npRandom;

var rng = new MT19937(4357 : uint(32));
var y : uint; // Note : test that upcasting works
for i in 0.. #1000 do y = rng.get();
writeln(y);
writeln(1186927261);
writeln(y==1186927261);
writeln(rng.get(real));

var rng2 = new Xorshift64star();
for i in 0.. #1000 do y = rng2.get();
writeln(y);

var rng3 = new Xorshift1024star();
for i in 0.. #1000 do y = rng3.get();
writeln(y);

var tot : real = 0.0;
config const N = 100000;
for i in 0.. #N do tot += rng2.get(real);
writeln(tot/N);
tot=0;
for i in 0.. #N do tot += rng3.get(real);
writeln(tot/N);
