// Define some parameters
config const NDIST=120;
config const NSAMP=300;
config const NNEIGH=8;

// Define some domains here -- again, for simplicity
// we assume all pixels look the same.
var Ddist = {0.. #NDIST};
var Dsamp = {0.. #NSAMP};
var Dneigh = {0.. #NNEIGH};


// Define a pixel data structure
record Pixel {
  // Sample array
  var sarr : [Dsamp][Ddist] real(32);
  var llike : [Dsamp] real(32);
  var neighndx : [Dneigh] int(32);
  
  var bestLoc : int;
}


// Define the update loop
proc updateOne(ipix : int, pixels : [] Pixel) : int(32) {
  var cpix = pixels[ipix]; // This is a copy
  var newLike = cpix.llike;
  var neigh :  Pixel;
  var nsamp : [Ddist] real(32);
  // Do this loop in serial, since we'll parallelize everything else
  for ineigh in cpix.neighndx {
    if (ineigh < 0) then continue; // Not a neighbour
    neigh = pixels[ineigh]; // This might be a remote fetch
    nsamp = neigh.sarr[neigh.bestLoc];
    forall isamp in Dsamp {
      newLike[isamp] -= + reduce (cpix.sarr[isamp]-nsamp)**2;
    }
  }
  return (maxloc reduce zip(newLike, Dsamp))(2) : int(32);
}

proc doOne(pixels : [] Pixel) {
  var newBest : [pixels.domain] int;
  // Probably a cleaner way to do this
  [ipix in pixels.domain] newBest[ipix] = updateOne(ipix, pixels); 
  pixels.bestLoc = newBest;
}

// I/O routines
// TODO : Why can't I specify an array return type
proc readPixels(fn : string) {
  var fr = open(fn, iomode.r).reader(kind=iokind.big);
  var npix, nsamp : int(32);
  fr.read(npix);
  fr.read(nsamp);
  writeln(npix, ' ',nsamp);
  if (nsamp != NSAMP) then halt("Unexpected number of samples");
  var lik : [{0.. #npix, 0.. #NSAMP}] real(32); fr.read(lik);
  writeln(lik[0,0], ' ',lik[0,1],' ',lik[1,0]);
  var samp : [{0.. #npix, 0.. #NSAMP, 0.. #NDIST}] real(32);
  fr.read(samp);
  var neigh : [{0.. #NNEIGH, 0.. #npix}] int(32); 
  fr.read(neigh);
  fr.close();


  // reorganize into pixels
  var pixels : [0.. #npix] Pixel;
  for ipix in pixels.domain {
    [i in Dsamp] pixels[ipix].sarr[i] = samp[ipix, i, ..];
    pixels[ipix].llike = lik[ipix,..];
    pixels[ipix].neighndx = neigh[..,ipix];
  }

  return pixels;
  
}

proc writePixels() {
}

    
proc main() {
  var pixels = readPixels("../data/samples_tiny.dat");

  doOne(pixels[0..10]);
}


