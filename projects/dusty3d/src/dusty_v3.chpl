use Time;

// SGEMM 
use SysCTypes;
extern proc npsgemm(const M : c_int, 
    const N : c_int, const K : c_int, 
    ref A : real(32), ref B : real(32),
    ref C : real(32));

// Put in parameters that control 
// what bits of code run and not
config param CoreOff=false;
config param UseBlas=false;

// Define some parameters
config const NDIST=120;
config const NSAMP=300;
config const NNEIGH=8;
config const NITER : int(32)=1;
config const INFILE="../data/samples_tiny.dat";
config const OUTFILE="../data/samples_tiny_bestloc.dat";

// Define some domains here -- again, for simplicity
// we assume all pixels look the same.
var npixels : int(32) = 0;
var Dsamp : domain(3);
var D2,Dneigh : domain(2);
var Dpix : domain(1);
var samp : [Dsamp] real(32);
var sqrs,llike : [D2] real(32);
var neighndx : [Dneigh] int(32);
var bestLoc : [Dpix] int(32);

// Global timer
var tt : Timer;

// Define the update loop
proc doOne(iiter : int)  {
  var newBest : [Dpix] int(32);

  // Current best samples
  var curBest : [0.. #npixels, 0.. #NDIST] real(32); // Distribute as pixels??
  var curBestSqrs : [Dpix] real(32);
  [ipix in Dpix] {
    curBest[ipix,..] = samp[ipix,bestLoc[ipix],..];
    curBestSqrs[ipix] = sqrs[ipix,bestLoc[ipix]];
  }

  forall ipix in Dpix {
    var newLike = llike[ipix,..];

    // collect the neighbours
    var narr : [0.. #NNEIGH, 0.. #NDIST] real(32);
    var nsqr : [0.. #NNEIGH] real(32);
    var numn=0;
    for ineigh in neighndx[ipix,..] {
      if (ineigh < 0) then continue; // Not a neighbour
      narr[numn,..] = curBest[ineigh,..];
      nsqr[numn] = curBestSqrs[ineigh];
      numn += 1;
    }
    
    // Loop over the neighbours
    if UseBlas then {
      var dx : [0.. #NSAMP, 0.. #NNEIGH] real(32);
      var dx1 : real(32);
      npsgemm(NSAMP : c_int, numn : c_int, NDIST : c_int, samp[ipix,0,0],narr[0,0],dx[0,0]);
      forall isamp in 0.. #NSAMP {
        dx1 = 0.0 : real(32);
        for ineigh in 0.. #numn {
          dx1 += (2 : real(32))*dx[isamp,ineigh]+nsqr[ineigh]+sqrs[ipix,isamp];
        }
        newLike[isamp] -= (iiter*2.5*dx1) : real(32); 
      }
    } else {
      var dx : real(32);
      for ineigh in 0.. #numn {
        forall isamp in {0.. #NSAMP} {
          dx = 0.0 : real(32);
          for ix in 0.. #NDIST {
            if (!CoreOff) then dx += (samp[ipix,isamp,ix]*narr[ineigh,ix]);
          }
          newLike[isamp] -= (iiter*2.5*(2*dx+sqrs[ipix,isamp]+nsqr[ineigh])) : real(32);
        }
      }
    }
    newBest[ipix] = (maxloc reduce zip(newLike, {0.. #NSAMP}))(2) : int(32);
  }

  bestLoc = newBest;
}

// I/O routines
proc readPixels(fn : string) {
  var fr = open(fn, iomode.r).reader(kind=iokind.big);
  var nsamp : int(32);
  fr.read(npixels);
  fr.read(nsamp);
  writeln(npixels, ' ',nsamp);
  if (nsamp != NSAMP) then halt("Unexpected number of samples");

  // Set the domains
  Dsamp = {0.. #npixels, 0.. #NSAMP, 0.. #NDIST};
  D2 = {0.. #npixels, 0.. #NSAMP};
  Dneigh = {0.. #npixels, 0.. #NNEIGH};
  Dpix = {0.. #npixels};


  fr.read(llike);
  writeln(llike[0,0], ' ',llike[0,1],' ',llike[1,0]);
  fr.read(samp);
  // Need to transpose this
  var neigh1 : [{0.. #NNEIGH, 0.. #npixels}] int(32); 
  fr.read(neigh1);
  forall (i,j) in neigh1.domain do neighndx[j,i] = neigh1[i,j];
  fr.close();

  // Set bestLoc
  forall ipix in Dpix {
    bestLoc[ipix] = (maxloc reduce zip(llike[ipix,..],{0.. #NSAMP}))(2) : int(32);
    forall isamp in {0.. #NSAMP} do
      sqrs[ipix,isamp] = + reduce (samp[ipix,isamp,..]**2);
  }

}



proc writePixels(fn : string, bestLocArr : [] int(32)) {
  var fw = open(fn, iomode.cw).writer(kind=iokind.little);
  for ii in bestLocArr.domain.dims() do fw.write(ii.length : int(32));
  fw.write(bestLocArr);
  fw.close();
}

    
proc main() {
  // Write expected data parallelism
  writeln("Expected maxTaskPar = ",here.maxTaskPar);

  readPixels(INFILE);

  // Define a bestLoc array
  var bestLocArr : [0.. #NITER, 0.. #npixels] int(32);

  tt.clear();
  tt.start();
  for iiter in {0.. #NITER} {
    doOne(iiter+1);
    bestLocArr[iiter,..] = bestLoc;
    writeln("Completed iteration ",iiter,' in elapsed time=',tt.elapsed());
  }

  writePixels(OUTFILE, bestLocArr);

}


