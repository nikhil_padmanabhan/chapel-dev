use SysCTypes;

var a : [1..2, 1..10] real(32);
var b : [1..1, 1..10] real(32);
var c : [1..2, 1..1] real(32);

a = 1.0 : real(32);
b = 2.0 : real(32);

extern proc npsgemm(const M : c_int, 
    const N : c_int, const K : c_int, 
    ref A : real(32), ref B : real(32),
    ref C : real(32));

writeln(a);
writeln(b);
npsgemm(2 : c_int, 1 : c_int, 10 : c_int, a[1,1], b[1,1],c[1,1]);
writeln(c);
