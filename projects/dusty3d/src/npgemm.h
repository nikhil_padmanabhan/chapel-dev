#include <cblas.h>

void npsgemm(const int M, const int N, const int K, const float *A, 
    const float *B, float *C);
