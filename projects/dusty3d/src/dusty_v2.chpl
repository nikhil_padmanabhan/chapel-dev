use Time;
use BlockDist;

// Define some parameters
config const NDIST=120;
config const NSAMP=300;
config const NNEIGH=8;
config const NITER : int(32)=1;
config const INFILE="../data/samples_tiny.dat";
config const OUTFILE="../data/samples_tiny_bestloc.dat";

// Define some domains here -- again, for simplicity
// we assume all pixels look the same.
var Ddist = {0.. #NDIST};
var Dsamp = {0.. #NSAMP};
var Dneigh = {0.. #NNEIGH};


// Define a pixel data structure
record Pixel {
  // Sample array
  var sarr : [0.. #NSAMP, 0.. #NDIST] real(32);
  var sqrs : [0.. #NSAMP] real(32);
  var llike : [Dsamp] real(32);
  var neighndx : [Dneigh] int(32);
  
  var bestLoc : int(32);
}

// Global timer
var tt : Timer;

// Define the update loop
proc doOne(iiter : int, pixels : [] Pixel)  {
  var newBest : [pixels.domain] int(32);

  // Current best samples
  var npix = pixels.numElements;
  var curBest : [0.. #npix, 0.. #NDIST] real(32); // Distribute as pixels??
  var curBestSqrs : [0.. #npix] real(32);
  [ipix in pixels.domain] {
    curBest[ipix,..] = pixels[ipix].sarr[pixels[ipix].bestLoc,..];
    curBestSqrs[ipix] = pixels[ipix].sqrs[pixels[ipix].bestLoc];
  }

  forall ipix in pixels.domain {
    var newLike = pixels[ipix].llike;
    var nsamp : [Ddist] real(32);

    // collect the neighbours
    var narr : [0.. #NNEIGH, 0.. #NDIST] real(32);
    var nsqr : [0.. #NNEIGH] real(32);
    var numn=0;
    for ineigh in pixels[ipix].neighndx {
      if (ineigh < 0) then continue; // Not a neighbour
      narr[numn,..] = curBest[ineigh,..];
      nsqr[numn] = curBestSqrs[ineigh];
      numn += 1;
    }
    
    // Loop over the neighbours
    var dx : real(32);
    for ineigh in 0.. #numn {
      var sarr = pixels[ipix].sarr;
      forall isamp in Dsamp {
        dx = 0.0 : real(32);
        for ix in 0.. #NDIST {
          dx += (sarr[isamp,ix]*narr[ineigh,ix]);
        }
        newLike[isamp] -= (iiter*2.5*(2*dx+pixels[ipix].sqrs[isamp]+nsqr[ineigh])) : real(32);
      }
    }
    newBest[ipix] = (maxloc reduce zip(newLike, Dsamp))(2) : int(32);
  }

  pixels.bestLoc = newBest;
}

// I/O routines
// TODO : Why can't I specify an array return type
proc readPixels(fn : string) {
  var fr = open(fn, iomode.r).reader(kind=iokind.big);
  var npix, nsamp : int(32);
  fr.read(npix);
  fr.read(nsamp);
  writeln(npix, ' ',nsamp);
  if (nsamp != NSAMP) then halt("Unexpected number of samples");
  var lik : [{0.. #npix, 0.. #NSAMP}] real(32); fr.read(lik);
  writeln(lik[0,0], ' ',lik[0,1],' ',lik[1,0]);
  var samp : [{0.. #npix, 0.. #NSAMP, 0.. #NDIST}] real(32);
  fr.read(samp);
  var neigh : [{0.. #NNEIGH, 0.. #npix}] int(32); 
  fr.read(neigh);
  fr.close();


  // reorganize into pixels
  var pixels : [0.. #npix] Pixel;
  writeln(pixels.numElements);
  for ipix in pixels.domain {
    pixels[ipix].sarr = samp[ipix, .., ..];
    pixels[ipix].llike = lik[ipix,..];
    pixels[ipix].neighndx = neigh[..,ipix];
    // Set bestLoc
    pixels[ipix].bestLoc = (maxloc reduce zip(pixels[ipix].llike, Dsamp))(2) : int(32);
    forall isamp in Dsamp do
      pixels[ipix].sqrs[isamp] = + reduce (pixels[ipix].sarr[isamp,..]**2);
  }

  return pixels;
  
}

proc writePixels(fn : string, bestLocArr : [] int(32)) {
  var fw = open(fn, iomode.cw).writer(kind=iokind.little);
  for ii in bestLocArr.domain.dims() do fw.write(ii.length : int(32));
  fw.write(bestLocArr);
  fw.close();
}

    
proc main() {
  // Write expected data parallelism
  writeln("Expected maxTaskPar = ",here.maxTaskPar);

  var pixels = readPixels(INFILE);

  // Define a bestLoc array
  var bestLocArr : [0.. #NITER, 0.. #pixels.numElements] int(32);

  var newBest : [pixels.domain] int(32);
  tt.clear();

  tt.start();
  for iiter in {0.. #NITER} {
    doOne(iiter+1,pixels);
    bestLocArr[iiter,..] = pixels.bestLoc;
    writeln("Completed iteration ",iiter,' in elapsed time=',tt.elapsed());
  }

  writePixels(OUTFILE, bestLocArr);

}


