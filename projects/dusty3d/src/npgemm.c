#include "npgemm.h"


void npsgemm(const int M, const int N, const int K, const float *A, 
    const float *B, float *C) {
  cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasTrans,
      M, N, K, 1.0, A, K, B, K, 0, C, N);
}
