use BlockCycDist;

const Space = {1..10,1..3};
var D : domain(2) dmapped BlockCyclic(startIdx=Space.low, blocksize = (4,3)) = Space;
var D1 : domain(2) dmapped BlockCyclic(startIdx=Space.low, blocksize=(4,3));
var A : [D] int;

forall a in A do a = a.locale.id;
writeln(A);

D1 = {1..15,1..6};
var A1 : [D1] int;

forall a in A1 do a = a.locale.id;
writeln(A1);
