from astropy.io import fits
import numpy as np

ff = fits.open("samples_tiny.fits")
tb = ff[1].data
ff.close()
outff = open("samples_tiny.dat","wb")
# Write out number of pixels and number of samples
n = np.array(tb['lnz'].shape,dtype='>i4')
print n
print n.dtype
n.tofile(outff)
print tb.columns
print tb['lnz'].shape
print tb['lnz'].dtype
print tb['lnz'][0,0], ' ',tb['lnz'][0,1], tb['lnz'][1,0]
tb['lnz'].tofile(outff)
print tb['samples'].shape
tb['samples'].tofile(outff)
ff = fits.open("neighbors_tiny.fits")
print ff[0].data.shape
ff[0].data.tofile(outff)
ff.close()
outff.close()



