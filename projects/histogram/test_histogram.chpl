use Histogram;
use Random;

config const n : int = 10;
config const w : real = 1.0;
config const nbins : int = 10;

proc test1d() {
  var hh = new UniformBins(1, (nbins,), ((0.0,nbins:real),));
  var arr : [0.. #(n*nbins)] real;
  fillRandom(arr);
  arr /= 10.0;
  [i in arr.domain] arr[i] += i % nbins;
  [x in arr] hh.add((x,),w);
  var errs = 0;
  for ii in hh.Dhist {
    if (abs(hh[ii]/(n*w)-1.0) > 1.0e-8) {
      writeln(hh[ii]," ",n*w, " ",hh[ii]-n*w);
      errs += 1;
    }
  }
  writeln(errs," errors detected");
}


proc test2d() {
  var hh = new UniformBins(2, (nbins,2), ((0.0,nbins:real),(0.0,1.0)));
  var arr : [0.. #(n*nbins)] real;
  fillRandom(arr);
  arr /= 10.0;
  [i in arr.domain] arr[i] += i % nbins;
  [x in arr] hh.add((x,0.3),w);
  [x in arr] hh.add((x,0.8),2*w);
  var errs = 0;
  for ii in hh.Dhist {
    if (abs(hh[ii]/(n*w*(ii(2)+1))-1.0) > 1.0e-8) {
      writeln(ii," ",hh[ii]," ",n*w, " ",hh[ii]-n*w);
      errs += 1;
    }
  }
  writeln(errs," errors detected");
}



proc main() {
  test1d();
  test2d();
}
