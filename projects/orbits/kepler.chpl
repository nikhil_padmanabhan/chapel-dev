/* Module with code for working out orbits. 

Nikhil Padmanabhan, Yale
Jan 2015
*/

module Kepler {
	
	const Pi=3.1415926535897932385;

	/* Solve for eccentric anomaly given the mean anomaly
	   using a Newton-Raphson iteration. 

	   See Murray and Correia, "Keplerian Orbits and Dynamics of Exoplanets"
	   in "Exoplanets", ed. Seager.

	   We start the iterations with E=M, except for 
	   eccentricities > 0.8, where we start with E=\pi.

	 */
	proc eccAnomaly(ecc : real, M : real, maxiter : int = 100, epsabs : real = 1.e-7) : real {
		var niter : int = 0;
		var E : real; // Eccentric anomaly
		if (ecc < 0.8) then E=M; else E=Pi;

		var err = E - ecc * sin(E) - M;
		while (niter < maxiter) && (abs(err) > epsabs) {
			// Compute the next iteration
			E = E-err/(1 - ecc*cos(E));
			err = E - ecc * sin(E) - M;
			niter += 1;
		}

		if (niter==maxiter) then halt("Too many iterations computing the eccentric anomaly");

		return E;
	}

	/* Convert eccentric to true anomaly

	   tan f/2 = \sqrt{(1+e)/(1-e)} \tan E/2

	   We use atan2 for stability in the conversion.
	*/
	proc ecc2true(ecc : real, E : real) : real {
		var Eh = E/2;
		return 2*atan2(sqrt(1+ecc)*sin(Eh), sqrt(1-ecc)*cos(Eh));
	}


	/* Orbit library class 

	   Cache true anomaly as a function of phase (mean anomaly) for RV calculations.

	*/
	class OrbitLibrary {
		// Dimensions
		var necc, nM : int(64); // Number of eccentricities and phases to store
		var Decc, DM : domain(1); // Domains of eccentricities
		var Dlib : domain(2); // Library domain
		var ecc : [Decc] real;
		var M : [DM] real;
		var lib : [Dlib] real;

		var decc, dphi : real;

		proc OrbitLibrary(necc : int, nM : int) {
			this.necc = necc;
			this.nM = nM;
			decc = 1.0/necc; 
			dphi = 1.0/nM;

			Decc = 0.. #necc;
			[i in Decc] ecc[i] = decc*i;
			DM = 0..nM; // Store 2 * Pi as well for simplicity while linear interpolation
			[i in DM] M[i] = (2.0*Pi*dphi) * i;
			Dlib = {0.. #necc, 0..nM}; // Can we do a cartesian product?

			// Generate library
			[(ie,ip) in Dlib] {
				var e1 = ecc[ie];
				lib[ie,ip] = ecc2true(e1, eccAnomaly(e1, M[ip]));
			}
		}

		proc OrbitLibrary(fn : string) {
			var fr = open(fn, iomode.r).reader(kind=iokind.little);
			fr.read(necc);
			fr.read(nM);
			decc = 1.0/necc; 
			dphi = 1.0/nM;

			Decc = 0.. #necc;
			DM = 0..nM; // Store 2 * Pi as well for simplicity while linear interpolation
			fr.read(ecc);
			fr.read(M);
			Dlib = {0.. #necc, 0..nM}; // Can we do a cartesian product?
			fr.read(lib);
			fr.close();
		}

		/* 
		   File format : 
		      necc, nM : int64
			  ecc[necc] : real64
			  M[nM+1] : real64 --- NOTE nM+1
			  lib[necc, nM+1] : real64--- NOTE nM+1
		*/
		proc save(fn : string) {
			var fr = open(fn, iomode.cw).writer(kind=iokind.little);
			fr.write(necc);
			fr.write(nM);
			fr.write(ecc); fr.write(M);
			fr.write(lib);
			fr.close();
		}


	}
}
