// Test program for the kepler module

use Kepler;

proc testEccAnomaly(ecc, M) {
	var	E = eccAnomaly(ecc, M);
	var diff = E - ecc*sin(E)-M;
	writeln("max diff =",max reduce abs(diff));
}

// Test out eccentric anomaly code
var ecc : real;
config const N = 1000;
var D = 0..N;
var M: [D] real;
var frac = 1.0/N;
[i in D] M[i]= (i*frac)*2*Pi;
writeln("Testing eccentric anomaly calculations.....");
testEccAnomaly(0.1,M);
testEccAnomaly(0.7,M);
testEccAnomaly(0.9,M);
testEccAnomaly(0.999,M);

var orbits = new OrbitLibrary(100,1000);
writeln("Orbit library built");
orbits.save("orbits.dat");
var orb2 = new OrbitLibrary("orbits.dat");
writeln(max reduce abs(orbits.lib - orb2.lib));
