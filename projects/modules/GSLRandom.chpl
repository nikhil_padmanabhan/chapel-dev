module GSLRandom {
  
  use SysCTypes;
  use "gsl/gsl_rng.h", "gsl/gsl_randist.h", "gsl/gsl_cdf.h";
  use "-lgsl", "-lgslcblas";

  extern type gsl_rng;
  extern type gsl_rng_type;

  type rng_ptr = c_ptr(gsl_rng);
  type rng_type_ptr = c_ptr(gsl_rng_type);

  extern proc gsl_rng_alloc(const rng_type_ptr) : rng_ptr;
  extern proc gsl_rng_free(r : rng_ptr);
  extern proc gsl_rng_set(r : rng_ptr, seed : c_ulonglong);
  extern proc gsl_rng_uniform(r : rng_ptr) : c_double;

  // Gaussians
  extern proc gsl_ran_gaussian(r : rng_ptr, sigma : c_double) : c_double;
  extern proc gsl_ran_gaussian_pdf(x : c_double, sigma : c_double) : c_double;
  extern proc gsl_ran_gaussian_P(x : c_double, sigma : c_double) : c_double;
  extern proc gsl_ran_gaussian_Q(x : c_double, sigma : c_double) : c_double;
  extern proc gsl_ran_gaussian_Pinv(P : c_double, sigma : c_double) : c_double;
  extern proc gsl_ran_gaussian_Qinv(Q : c_double, sigma : c_double) : c_double;
  

  // Random number generation routines...
  // Note that the are all set as c_void_ptr because I can't get Chapel to 
  // work with the correct type.
  extern const gsl_rng_mt19937 : rng_type_ptr;
 
  // Helper routine
  record RandomGen {
    var r : rng_ptr;

    proc RandomGen(seed : uint(64)=0) {
      r = gsl_rng_alloc(gsl_rng_mt19937);
      if (seed != 0) then gsl_rng_set(r, seed : c_ulonglong);
    }

    proc ~RandomGen() {
      gsl_rng_free(r);
    }

    proc uniform() : real(64) {
      return gsl_rng_uniform(r) : real(64);
    }

    proc gaussian(sigma : real(64)=1) : real(64) {
      return gsl_ran_gaussian(r, sigma) : real(64);
    }

  }

}
