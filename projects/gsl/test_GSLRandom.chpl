use GSLRandom;

config const Seed : uint(64) = 0;

var rr = new RandomGen(Seed);

for ii in 1..10 {
  writeln(rr.uniform(),"  ",rr.gaussian(),"  ",rr.gaussian(0.1));
}
