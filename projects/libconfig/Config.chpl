/* Wrapping the libconfig C API 
   in order to have a simple way to read
   in configuration files.

   Nikhil Padmanabhan, 
   Yale
   May 2015
   */


module Config {

 use SysCTypes;
 use "libconfig_fudge.h","-lconfig";

 // Constants
 extern const CONFIG_TRUE : c_int;
 extern const CONFIG_FALSE : c_int;

 // Types
 extern type config_t;
 extern type config_setting_t;
 type configptr = c_ptr(config_t);
 type settingptr = c_ptr(config_setting_t);

 // C API
 extern proc config_init(ref cfg : config_t);
 extern proc config_destroy(ref cfg : config_t);
 extern proc config_read_file(ref cfg : config_t, filename : c_string) : c_int;

 // Direct lookups
 extern proc config_lookup_int(ref cfg : config_t, path : c_string, 
     ref ret : c_int) : c_int;
 extern proc config_lookup_int64_v2(ref cfg : config_t, path : c_string, 
     ref ret : c_longlong) : c_int;
 extern proc config_lookup_float(ref cfg : config_t, path : c_string, 
     ref ret : c_double) : c_int;
 extern proc config_lookup_bool(ref cfg : config_t, path : c_string, 
     ref ret : c_int) : c_int;
 extern proc config_lookup_string(ref cfg : config_t, path : c_string, 
     ref ret : c_string) : c_int;
 extern proc config_lookup(ref cfg : config_t, path : c_string) : settingptr;

 // Settings
 extern proc config_setting_index(set : settingptr) : c_int;
 extern proc config_setting_length(set : settingptr) : c_int;
 extern proc config_setting_is_array(set : settingptr) : c_int;
 extern proc config_setting_is_list(set : settingptr) : c_int;

 proc readConfigFile(fn : c_string) : ConfigFile {
   var c = new ConfigFile();
   var ret = config_read_file(c.cfg,fn);
   if (ret == CONFIG_TRUE) then return c; else return nil;
 }

 // Chapel API
 record ConfigSetting {
   var setting : settingptr;

   proc pos() : int {
     return config_setting_index(setting) : int;
   }

   proc length() : int {
     return config_setting_length(setting) : int;
   }

   proc this(type t, i : int) : t {
     var ret : t;
     if (config_setting_is_list(setting)==CONFIG_FALSE) &&
       (config_setting_is_array(setting)==CONFIG_FALSE) then return ret;
     return ret;
   }
 }
  
 class ConfigFile {
   var cfg : config_t;   


   proc reset() {
     config_destroy(cfg);
   }

   proc ConfigFile() {
     config_init(cfg);
   }

   proc ~ConfigFile() {
     reset();
   }

   proc lookup(path : string) : ConfigSetting {
     var s : ConfigSetting;
     s.setting = config_lookup(cfg, path.c_str());
     return s;
   }

   proc lookup(type t, path : string) : (t, bool) {
     var ret : t;
     var good = false;
     select t {
       when int,int(64) {
         var ret1 : c_longlong;
         good = (config_lookup_int64_v2(cfg, path.c_str(),ret1)==CONFIG_TRUE);
         //writeln("Internal error -- int(64) is not correctly implemented");
         ret = ret1 : int(64);
       }
       when int(32) {
         var ret1 : c_int;
         good = (config_lookup_int(cfg, path.c_str(),ret1)==CONFIG_TRUE);
         ret = ret1 : int(32);
       }
       when real {
         var ret1 : c_double;
         good = (config_lookup_float(cfg, path.c_str(),ret1)==CONFIG_TRUE);
         ret = ret1 : real;
       }
       when string {
         var ret1 : c_string;
         good = (config_lookup_string(cfg, path.c_str(),ret1)==CONFIG_TRUE);
         ret = ret1 : string;
       }
       when bool {
         var ret1 : c_int;
         good = (config_lookup_bool(cfg, path.c_str(),ret1)==CONFIG_TRUE);
         ret = ret1==CONFIG_TRUE;
       }
       otherwise {
         writef("Error : Unknown type \n");
       }
     }
      
     return (ret, good);

   }

       

 }



}
