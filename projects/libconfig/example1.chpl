use Config;

proc main() {
  var cfg = readConfigFile("example.cfg");

  // Exercise direct lookup
  writeln(cfg.lookup(int,"hours.mon.open"));
  writeln(cfg.lookup(int(32),"hours.mon.close"));
  writeln(cfg.lookup(string, "name"));
  writeln(cfg.lookup(real, "pi"));
  writeln(cfg.lookup(real, "euler"));
  writeln(cfg.lookup(bool, "isGood"));

  var s1 = cfg.lookup("inventory.books");
  if (s1.setting != nil) {
    writeln("Index of books is=", s1.pos());
    writeln("Length of books is=", s1.length());
  } else {
    writeln("unable to find books");
  }
  
  s1 = cfg.lookup("inventory.movies");
  if (s1.setting != nil) {
    writeln("Index of movies is=", s1.pos());
    writeln("Length of movies is=", s1.length());
  } else {
    writeln("unable to find movies");
  }

  writeln("Arrays..");
  s1 = cfg.lookup("anArray");
  if (s1.setting!=nil) {
    writeln("Length=",s1.length());
  } else {
    writeln("No array found...");
  }
  
  writeln("Lists...");
  s1 = cfg.lookup("aList");
  if (s1.setting!=nil) {
    writeln("Length=",s1.length());
  } else {
    writeln("No list found...");
  }
}

