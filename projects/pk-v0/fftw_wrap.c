#include <fftw3.h>

// Convenience wrapper -- assumes a 3D, inplace transform, 
// equal dimensions in all directions, and FFTW_ESTIMATE
fftw_plan make_r2c_plan(int n0, double *in) {
	return fftw_plan_dft_r2c_3d(n0,n0,n0,in,(fftw_complex*) in,FFTW_ESTIMATE);
}


