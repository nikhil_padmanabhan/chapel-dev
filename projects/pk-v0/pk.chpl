use IO;
use SysCTypes;
use Math;
use Time;

config const N = 512;
config const Lbox=250.0;
const Vol=Lbox**3;
const Pi=3.1415926535897932385;
const k0=2*Pi/Lbox;

// Histogram config
config const kmin=0.03;
config const dk=0.01;
config const Nk=50;


// External FFTW calls
extern proc make_r2c_plan(n0 : c_int, ref x : real(64)) : opaque;
extern proc fftw_execute(plan : opaque);
extern proc fftw_destroy_plan(plan : opaque);


proc readFile(filename, grid : []real(64)) {
	// Open the file
	var ff = open(filename, iomode.r);
	var fr = ff.reader();

	var hdr : string;
	var ok : bool = fr.readln(hdr);
	if (!ok) {
		writeln("Failed to read the header");
		exit(1);
	}

	var iline = 0;
	var id,ii,jj,kk : int(64);
	var dens : real(64);
	while (ok) {
		ok = fr.readf("%u,%u,%u,%u,%r\n",id,ii,jj,kk,dens);
		if (!ok) then break;
		grid[ii,jj,kk]= dens;
		if (iline%(1024*1024)==0) then writeln("Processed :",iline/(1024*1024)," M lines");
		iline+=1;
	}


	fr.close();
	ff.close();
}




// We will just do everything in main for simplicity
proc main(args : [] string) {
	var Dcomplex : domain(3) = {0.. #N, 0.. #N, 0.. #(N+2)};
	var Dreal : domain(3) = {0.. #N, 0.. #N, 0.. #N};
	var grid : [Dcomplex] real(64);

	writeln("This locale supports maxTasksPar=",here.maxTaskPar);

	// Make the plan
	var plan : opaque = make_r2c_plan(N : c_int, grid[0,0,0]);

	writeln("Reading in file ",args[1]);
	readFile(args[1], grid);

	writeln("Grid sum = ", + reduce grid[Dreal]);
	fftw_execute(plan);

	// Normalize the grid -- FFTW computes an unnormalized transform
	grid /= (N : real(64)) **3;

	//  Define the histogram data 
	var kmean : [Nk] atomic real(64);
	var Pk : [Nk] atomic real(64);
	var Nmodes : [Nk] atomic real(64);

	// Define the domain we're going to run over
	var Dc2 : domain(3) = {0.. #N, 0.. #N, 0.. #(N/2+1)};

	var tt : Timer;
	tt.clear();
	tt.start();
	forall (ii,jj,kk) in Dc2 {
		var kx,ky,kz,fac,kval,Pk0: real(64);
		var ik : int;
		kx = if (ii > N/2) then (N-ii) else ii;
		ky = if (jj > N/2) then (N-jj) else jj;
		kz = kk : real(64);
		fac = if ((kk==0) || (kk==N/2)) then 1 else 2;
		// Note that kz is never greater than N/2 by construction
		kval = sqrt(kx*kx + ky*ky + kz*kz) * k0;
		ik = floor((kval-kmin)/dk) : int;
		Pk0 = grid[ii,jj,2*kk]**2 + grid[ii,jj,2*kk+1]**2;
		(kmean[ik]).add(kval*fac);
		(Pk[ik]).add(Pk0*fac);
		(Nmodes[ik]).add(fac);
	}
	tt.stop();
	writeln("Pk calculation loop took =",tt.elapsed());

	var ff = open(args[2], iomode.cw);
	var fr = ff.writer();

	for ii in 0.. #Nk {
		var fac = Nmodes[ii].read();
		fr.writeln(ii," ",kmean[ii].read()/fac," ",Pk[ii].read()*Vol/fac," ",fac);
	}

	fr.close(); ff.close();




	fftw_destroy_plan(plan);
}


