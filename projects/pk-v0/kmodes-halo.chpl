use IO;
use SysCTypes;
use Math;
use Time;

config const N = 512;
config const Lbox=250.0;
config const kmax=0.1;
const Vol=Lbox**3;
const Pi=3.1415926535897932385;
const k0=2*Pi/Lbox;
const kmin=1.e-8; // Just to eliminate 0



// External FFTW calls
extern proc make_r2c_plan(n0 : c_int, ref x : real(64)) : opaque;
extern proc fftw_execute(plan : opaque);
extern proc fftw_destroy_plan(plan : opaque);


proc readFile(filename, grid : []real(64)) {
	grid =0;
	// Open the file
	var ff = open(filename, iomode.r);
	var fr = ff.reader();


	var iline = 0;
	var ok = true;
	var ii,jj,kk : int;
	var x,y,z,wt,mass,vcirc : real(64);
	var npart:real= 0;
	while (ok) {
		ok = fr.readf("%r %r %r %r %r %r",x,y,z,wt,mass,vcirc);
		if (!ok) then break;
		ii = round(x*N/Lbox) : int; if (ii >= N) then ii -= N;
		jj = round(y*N/Lbox) : int; if (jj >= N) then jj -= N;
		kk = round(z*N/Lbox) : int; if (kk >= N) then kk -= N;
		grid[ii,jj,kk] += 1;
		npart += 1;
		if (iline%(1024*1024)==0) then writeln("Processed :",iline/(1024*1024)," M lines");
		iline+=1;
	}


	fr.close();
	ff.close();


	npart /= (N**3);
	grid = grid/npart-1;
}




// We will just do everything in main for simplicity
proc main(args : [] string) {
	var Dcomplex : domain(3) = {0.. #N, 0.. #N, 0.. #(N+2)};
	var Dreal : domain(3) = {0.. #N, 0.. #N, 0.. #N};
	var grid : [Dcomplex] real(64);

	writeln("This locale supports maxTasksPar=",here.maxTaskPar);

	// Make the plan
	var plan : opaque = make_r2c_plan(N : c_int, grid[0,0,0]);

	writeln("Reading in file ",args[1]);
	readFile(args[1], grid);

	writeln("Grid sum = ", + reduce grid[Dreal]);
	fftw_execute(plan);

	// Normalize the grid -- FFTW computes an unnormalized transform
	grid /= (N : real(64)) **3;

	// Define the domain we're going to run over
	var Dc2 : domain(3) = {0.. #N, 0.. #N, 0.. #(N/2+1)};

	// Write out the lowest modes for later processing
	var ff = open(args[2], iomode.cw);
	var fr = ff.writer();
	for (ii,jj,kk) in Dc2 {
		var kx,ky,kz,fac,kval,Pk0: real(64);
		var ik : int;
		kx = if (ii > N/2) then (N-ii) else ii;
		ky = if (jj > N/2) then (N-jj) else jj;
		kz = kk : real(64);
		fac = if ((kk==0) || (kk==N/2)) then 1 else 2;
		// Note that kz is never greater than N/2 by construction
		kval = sqrt(kx*kx + ky*ky + kz*kz) * k0;
		if ((kval < kmin) || (kval > kmax)) then continue;
		fr.writeln(ii," ",jj," ",kk," ",kval," ",grid[ii,jj,2*kk]," ",grid[ii,jj,2*kk+1]);
	}
	fr.close(); ff.close();

	fftw_destroy_plan(plan);
}


