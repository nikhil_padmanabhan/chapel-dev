// A simple wrapper around arrays that supports dynamic resizing
module Vector {

	record Vector {
		type eltType;
		var len, cap : int;
		var D : domain(1);
		var arr : [D] eltType;

		// Constructor
		proc Vector(type eltType, len : int = 0, cap : int = 0) {
			this.len = len;
			this.cap = cap;
			if (cap < len) then this.cap = len;
			D = {0.. #this.cap};
		}


		// Indexing
		proc this(i : int) ref {
			if (i >= len) then halt("Index out of bounds");
			return arr[i];
		}

		// Push-back
		proc push_back(x : eltType) {
			if (len >= cap) {
				cap = 2*cap;
				D = {0.. #cap};
			}
			arr[len] = x;
			len += 1;
		}

	} // Vector

}
