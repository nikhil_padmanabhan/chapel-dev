use Vector; 

var a : Vector(real);
writeln(a);
var b = new Vector(real,5);
writeln(b);
var c = new Vector(real,5,10);
writeln(c);


b[4] = 10.0;
writeln(b);
b.push_back(7); // Triggers a reallocation
writeln(b);
c[3] = 10;
// These don't trigger reallocations
c.push_back(8);
c.push_back(12.0);
writeln(c);


