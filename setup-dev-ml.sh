env | grep CHPL | sed 's/=.*//' | while read i; do unset $i; done
export NP_CHPL_MODULES=`pwd`/projects/modules
cd chapel; source util/setchplenv.bash; cd ..
export CHPL_HWLOC=none
# This is for Parallels on my Mac
export CHPL_TARGET_ARCH=none
#export CHPL_REGEXP=none
#export CHPL_DEVELOPER=true
export CHPL_COMM=gasnet
export CHPL_COMM_SUBSTRATE=mpi

