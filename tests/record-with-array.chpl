record Vector {
	var D = {0.. #3};
	var A : [D] real;
	var cap : int;
}

var p1 : Vector;
p1.A[0] = 10;
p1.cap = 5;
writeln(p1);

var p2 = p1;
writeln(p2);

p1.A[1]=7;
p1.cap = 9;
writeln(p1);
writeln(p2);

var A : [0..5] real;
A[3] = 8;
writeln(A);
var B = A;
writeln(B);
A[4] = 9;
writeln(A);
writeln(B);

