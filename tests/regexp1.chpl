use Regexp;

// Comment stripping!
//var re = compile("([^#]*)#.*");
var re = compile("#.*");
writeln(re);

writeln(re.sub("","This is a not a comment"));
writeln(re.sub("","   # This should be recognized as a comment"));
writeln(re.sub("","tstss hdjsj #so should this."));
writeln(re.sub(""," 3#4"));
writeln(re.sub(""," 3#4#5"));

// Split on whitespace
var white = compile("[[:space:]]+");

var s1 = " sdjstab   fsjkjks    shdjd jwwhs";
var s2 = "      ";
writeln("One");
for ss in white.split(s1) {
	if (ss.length==0) then continue;
	writeln(ss);
}
writeln("Two");
for ss in white.split(s2) {
	if (ss.length==0) then continue;
	writeln(ss);
}
writeln("Done");

// Split tests
var s3 = ",,,,1,2,3,\n,4,,,";
var comma = compile("[\\n,]");
writeln("Comma Start");
for ss in comma.split(s3) {
	if (ss.length==0) then continue;
	writeln(ss);
}
writeln("Comma done");
