// Does this race or not

var i : int = 10;
var A : [0..100] int;

[a in A] {
	a = i;
	i=i+1;
}

writeln(A);
i=10;

var B : [0..100] int;
for b in B {
	b=i;
	i=i+1;
}

writeln(B);

i=10;
proc f(x : int) : int {
	i = i+1;
	return i;
}
writeln(f(7));
writeln(f(8));

A = f(B);
writeln(A);
