env | grep CHPL | sed 's/=.*//' | while read i; do unset $i; done
cd releases/chapel-1.11.0; source util/setchplenv.bash
export CHPL_HWLOC=none
cd ../..
